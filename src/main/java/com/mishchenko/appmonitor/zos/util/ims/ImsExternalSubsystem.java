package com.mishchenko.appmonitor.zos.util.ims;

import java.io.Serializable;

public class ImsExternalSubsystem implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;

	private String status;

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	@Override
	public String toString() {
		return "ImsExternalSubsystem{" + "name='" + getName() + "', status='" + getStatus() + "'}";
	}
}