package com.mishchenko.appmonitor.zos.service.impl;

import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;

import com.mishchenko.appmonitor.domain.Issue;
import com.mishchenko.appmonitor.domain.Subsystem;
import com.mishchenko.appmonitor.domain.SubsystemInformation;
import com.mishchenko.appmonitor.domain.SubsystemJob;
import com.mishchenko.appmonitor.repository.SubsystemJobRepository;
import com.mishchenko.appmonitor.repository.SubsystemRepository;
import com.mishchenko.appmonitor.zos.connection.ImsConnection;
import com.mishchenko.appmonitor.zos.connection.JesConnection;
import com.mishchenko.appmonitor.zos.service.ConnectionFactoryService;
import com.mishchenko.appmonitor.zos.service.IssueNotificationService;
import com.mishchenko.appmonitor.zos.service.IssueUpdateService;
import com.mishchenko.appmonitor.zos.service.SubsystemUpdateService;
import com.mishchenko.appmonitor.zos.util.ZObject;
import com.mishchenko.appmonitor.zos.util.ims.ImsExternalSubsystem;
import com.mishchenko.appmonitor.zos.util.ims.ImsQueuedMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service implementation for updating IMS subsystem objects and information.
 */
@Service
@Transactional
public class SubsystemUpdateServiceImpl implements SubsystemUpdateService {

	private static final Logger log = LoggerFactory.getLogger(SubsystemUpdateServiceImpl.class);

	private final ConnectionFactoryService connectionFactoryService;

	private final IssueUpdateService issueUpdateService;

	private final IssueNotificationService issueNotificationService;

	private final SubsystemRepository subsystemRepository;

	private final SubsystemJobRepository subsystemJobRepository;

	public SubsystemUpdateServiceImpl(ConnectionFactoryService connectionFactoryService,
			IssueUpdateService issueUpdateService, IssueNotificationService issueNotificationService,
			SubsystemRepository subsystemRepository, SubsystemJobRepository subsystemJobRepository) {
		this.connectionFactoryService = connectionFactoryService;
		this.issueUpdateService = issueUpdateService;
		this.issueNotificationService = issueNotificationService;
		this.subsystemRepository = subsystemRepository;
		this.subsystemJobRepository = subsystemJobRepository;
	}

	@Override
	public void updateIssues(Subsystem subsystem) throws Exception {
		log.info("Updating IMS transactions, programs and databases for: {}", subsystem);
		ImsConnection imsConnection = connectionFactoryService.getImsConnection(subsystem);
		imsConnection.connect();
		try {
			List<ZObject> imsObjects = new LinkedList<>();
			imsObjects.addAll(imsConnection.getTransactionStatuses());
			imsObjects.addAll(imsConnection.getProgramStatuses());
			imsObjects.addAll(imsConnection.getDatabaseStatuses());
			log.info("Received {} IMS objects: {}", imsObjects.size(), imsObjects);
			List<Issue> updatedIssues = issueUpdateService.checkObjects(imsObjects, subsystem);
			log.info("Created {} new issues: {}", updatedIssues.size(), updatedIssues);
			subsystem.setUpdatedAt(ZonedDateTime.now());
			subsystemRepository.save(subsystem);
			issueNotificationService.notify(updatedIssues);
		} catch (Exception e) {
			log.error("Error while updating issues for: {}. Error message: {}", subsystem, e.getMessage());
		} finally {
			imsConnection.disconnect();
		}
	}

	@Override
	public void updateInformation(Subsystem subsystem) throws Exception {
		log.info("Updating IMS queued messages and external subsystem for: {}", subsystem);
		ImsConnection imsConnection = connectionFactoryService.getImsConnection(subsystem);
		imsConnection.connect();
		try {
			List<ImsExternalSubsystem> imsExternalSubsystems = imsConnection.getExternalSubsystems();
			log.debug("Received {} IMS external subsystems: {}", imsExternalSubsystems.size(), imsExternalSubsystems);
			List<ImsQueuedMessage> imsQueuedMessages = imsConnection.getQueuedMessages();
			log.debug("Received {} IMS queued messages: {}", imsQueuedMessages.size(), imsQueuedMessages);
			SubsystemInformation subsystemInformation = subsystem.getInformation();
			if (subsystemInformation == null) {
				subsystemInformation = new SubsystemInformation();
				subsystem.setInformation(subsystemInformation);
			}
			subsystemInformation.setImsExternalSubsystems(imsExternalSubsystems);
			subsystemInformation.setImsQueuedMessages(imsQueuedMessages);
			this.subsystemRepository.save(subsystem);
			log.debug("Subsystem information updated for: {}", subsystem);
		} catch (Exception e) {
			log.error("Error while updating information for: {}. Error message: {}", subsystem, e.getMessage());
		} finally {
			imsConnection.disconnect();
		}
	}

	@Override
	public void updateJobStatuses(Subsystem subsystem) throws Exception {
		log.info("Updating subsystem jobs for: {}", subsystem);
		JesConnection jesConnection = connectionFactoryService.getJesConnection(subsystem);
		jesConnection.login();
		try {
			List<SubsystemJob> subsystemJobs = subsystemJobRepository.findAllBySubsystemId(subsystem.getId());
			subsystemJobs = jesConnection.getJobsInformation(subsystemJobs);
			subsystemJobRepository.save(subsystemJobs);
			log.debug("Subsystem jobs updated for: {}", subsystem);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			jesConnection.disconnect();
		}
	}
}
