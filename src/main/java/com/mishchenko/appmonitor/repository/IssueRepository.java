package com.mishchenko.appmonitor.repository;

import java.util.List;

import com.mishchenko.appmonitor.domain.Issue;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Issue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IssueRepository extends JpaRepository<Issue, Long> {

    @Query("select distinct issue from Issue issue where issue.subsystemObject.subsystem.id = :id")
    List<Issue> findAllBySubsystemId(@Param("id") Long id);

    @Query("select issue from Issue issue where issue.assignedUser.login = ?#{principal.username}")
    List<Issue> findByAssignedUserIsCurrentUser();
}
