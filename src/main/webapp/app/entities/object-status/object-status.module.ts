import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppmonitorSharedModule } from '../../shared';
import {
    ObjectStatusService,
    ObjectStatusPopupService,
    ObjectStatusComponent,
    ObjectStatusDetailComponent,
    ObjectStatusDialogComponent,
    ObjectStatusPopupComponent,
    ObjectStatusDeletePopupComponent,
    ObjectStatusDeleteDialogComponent,
    objectStatusRoute,
    objectStatusPopupRoute,
} from './';

const ENTITY_STATES = [
    ...objectStatusRoute,
    ...objectStatusPopupRoute,
];

@NgModule({
    imports: [
        AppmonitorSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ObjectStatusComponent,
        ObjectStatusDetailComponent,
        ObjectStatusDialogComponent,
        ObjectStatusDeleteDialogComponent,
        ObjectStatusPopupComponent,
        ObjectStatusDeletePopupComponent,
    ],
    entryComponents: [
        ObjectStatusComponent,
        ObjectStatusDialogComponent,
        ObjectStatusPopupComponent,
        ObjectStatusDeleteDialogComponent,
        ObjectStatusDeletePopupComponent,
    ],
    providers: [
        ObjectStatusService,
        ObjectStatusPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppmonitorObjectStatusModule {}
