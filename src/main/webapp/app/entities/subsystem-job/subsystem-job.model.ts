import { BaseEntity } from './../../shared';

export enum JesJobStatus {
    ACVITE = 'ACTIVE',
    STOPPED = 'STOPPED'
}

export enum SubsystemJobType {
    IMS_CONNECT = 'IMS_CONNECT',
    IMS_CONTROL_REGION = 'IMS_CONTROL_REGION',
    IMS_MPR = 'IMS_MPR'
}

export class SubsystemJob implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public status?: JesJobStatus,
        public type?: SubsystemJobType,
        public startedAt?: any,
        public log?: any,
        public subsystemId?: number,
    ) {
    }
}
