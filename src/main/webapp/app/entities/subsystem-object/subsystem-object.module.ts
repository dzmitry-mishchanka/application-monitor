import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppmonitorSharedModule } from '../../shared';
import {
    SubsystemObjectService,
    SubsystemObjectPopupService,
    SubsystemObjectComponent,
    SubsystemObjectDetailComponent,
    SubsystemObjectDialogComponent,
    SubsystemObjectPopupComponent,
    SubsystemObjectDeletePopupComponent,
    SubsystemObjectDeleteDialogComponent,
    subsystemObjectRoute,
    subsystemObjectPopupRoute,
    SubsystemObjectResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...subsystemObjectRoute,
    ...subsystemObjectPopupRoute,
];

@NgModule({
    imports: [
        AppmonitorSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SubsystemObjectComponent,
        SubsystemObjectDetailComponent,
        SubsystemObjectDialogComponent,
        SubsystemObjectDeleteDialogComponent,
        SubsystemObjectPopupComponent,
        SubsystemObjectDeletePopupComponent,
    ],
    entryComponents: [
        SubsystemObjectComponent,
        SubsystemObjectDialogComponent,
        SubsystemObjectPopupComponent,
        SubsystemObjectDeleteDialogComponent,
        SubsystemObjectDeletePopupComponent,
    ],
    providers: [
        SubsystemObjectService,
        SubsystemObjectPopupService,
        SubsystemObjectResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppmonitorSubsystemObjectModule {}
