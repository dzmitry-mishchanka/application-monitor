import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ObjectType } from './object-type.model';
import { ObjectTypePopupService } from './object-type-popup.service';
import { ObjectTypeService } from './object-type.service';

@Component({
    selector: 'jhi-object-type-delete-dialog',
    templateUrl: './object-type-delete-dialog.component.html'
})
export class ObjectTypeDeleteDialogComponent {

    objectType: ObjectType;

    constructor(
        private objectTypeService: ObjectTypeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.objectTypeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'objectTypeListModification',
                content: 'Deleted an objectType'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-object-type-delete-popup',
    template: ''
})
export class ObjectTypeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private objectTypePopupService: ObjectTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.objectTypePopupService
                .open(ObjectTypeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
