package com.mishchenko.appmonitor.zos.connection;

import static org.junit.Assert.assertEquals;

import java.util.List;

import com.mishchenko.appmonitor.zos.util.ZConnectionParameters;
import com.mishchenko.appmonitor.zos.util.ZObject;

import org.junit.Before;
import org.junit.Test;

public class ImsConnectionIntTest {

    private ZConnectionParameters connectionParameters;

    @Before
    public void createParameters() {
        this.connectionParameters = new ZConnectionParameters();
        connectionParameters.setHostname("172.20.2.116");
        connectionParameters.setPort("7003");
        connectionParameters.setUserId("LAPUSHA");
        connectionParameters.setPassword("LAPUSHA2");
        connectionParameters.setImsDatastore("IVP1");
        connectionParameters.setImsExitIdentifier("*SAMPL1*");
    }

    @Test
    public void testTransactionStatuses() throws Exception {
        ImsConnection imsConnection = new ImsConnection(connectionParameters);
        imsConnection.connect();
        List<ZObject> zObjects = imsConnection.getTransactionStatuses();
        imsConnection.disconnect();
        System.out.println("Received IMS transactions: " + zObjects);
        assertEquals(zObjects.get(0).getType(), "IMS_TRANSACTION");
    }

    @Test
    public void testDatabaseStatuses() throws Exception {
        ImsConnection imsConnection = new ImsConnection(connectionParameters);
        imsConnection.connect();
        List<ZObject> zObjects = imsConnection.getDatabaseStatuses();
        imsConnection.disconnect();
        System.out.println("Received IMS databases: " + zObjects);
        assertEquals(zObjects.get(0).getType(), "IMS_DATABASE");
    }

    @Test
    public void testProgramStatuses() throws Exception {
        ImsConnection imsConnection = new ImsConnection(connectionParameters);
        imsConnection.connect();
        List<ZObject> zObjects = imsConnection.getProgramStatuses();
        imsConnection.disconnect();
        System.out.println("Received IMS programs: " + zObjects);
        assertEquals(zObjects.get(0).getType(), "IMS_PROGRAM");
    }

    @Test
    public void testExecuteCommand() throws Exception {
        ImsConnection imsConnection = new ImsConnection(connectionParameters);
        imsConnection.connect();
        String response = imsConnection.execute("/DIS TRAN ALL");
        imsConnection.disconnect();
        System.out.println("Command response: " + response);
    }
}
