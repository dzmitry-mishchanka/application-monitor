package com.mishchenko.appmonitor.zos.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.commons.io.IOUtils;

public class ZJobUtil {

    private static final String JES_JOB_STATUSES = "jes-job-statuses.jcl";
    private static final String JES_JOB_LOGS = "jes-job-logs.jcl";

    public static InputStream prepareJesJobStatusesJcl(List<String> jobNames,
            ZConnectionParameters connectionParameters) throws IOException {
        InputStream inputStream = ZJobUtil.class.getClassLoader().getResourceAsStream("jcl/" + JES_JOB_STATUSES);
        String jclCode = IOUtils.toString(inputStream, "UTF-8");
        jclCode = prepareCommonJcl(jclCode, connectionParameters);
        StringBuilder jobNamesString = new StringBuilder("");
        for (String jobName : jobNames) {
            jobNamesString.append(jobName);
            jobNamesString.append("\n");
        }
        jobNamesString.deleteCharAt(jobNamesString.length() - 1);
        jclCode = jclCode.replaceAll("@JOB_NAMES@", jobNamesString.toString());
        return IOUtils.toInputStream(jclCode, "UTF-8");
    }

    private static String prepareCommonJcl(String jclCode, ZConnectionParameters connectionParameters) {
        jclCode = jclCode.replaceAll("@USERID@", connectionParameters.getUserId());
        jclCode = jclCode.replaceAll("@CLASS@", connectionParameters.getJobClass());
        jclCode = jclCode.replaceAll("@ACCINFO@", connectionParameters.getJobAccountingInfo());
        jclCode = jclCode.replaceAll("@SUBSYS@", connectionParameters.getSubsystem().getName());
        return jclCode;
    }
}
