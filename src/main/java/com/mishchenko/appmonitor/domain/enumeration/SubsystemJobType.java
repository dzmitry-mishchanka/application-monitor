package com.mishchenko.appmonitor.domain.enumeration;

/**
 * The SubsystemJobType enumeration.
 */
public enum SubsystemJobType {
    IMS_CONNECT, IMS_CONTROL_REGION, IMS_MPR
}
