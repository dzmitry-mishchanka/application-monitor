package com.mishchenko.appmonitor.zos.service;

import java.util.List;

import com.mishchenko.appmonitor.domain.Issue;
import com.mishchenko.appmonitor.domain.Subsystem;
import com.mishchenko.appmonitor.zos.util.ZObject;

import org.springframework.stereotype.Service;

/**
 * Service Interface for processing issues.
 */
@Service
public interface IssueUpdateService {

  List<Issue> checkObjects(List<ZObject> zObjects, Subsystem subsystem) throws Exception;
}
