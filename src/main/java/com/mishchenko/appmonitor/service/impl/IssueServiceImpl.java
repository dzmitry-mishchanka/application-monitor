package com.mishchenko.appmonitor.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import com.mishchenko.appmonitor.domain.Issue;
import com.mishchenko.appmonitor.repository.IssueRepository;
import com.mishchenko.appmonitor.service.IssueService;
import com.mishchenko.appmonitor.service.dto.IssueDTO;
import com.mishchenko.appmonitor.service.mapper.IssueMapper;
import com.mishchenko.appmonitor.zos.service.CommandExecuteService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Issue.
 */
@Service
@Transactional
public class IssueServiceImpl implements IssueService {

    private final Logger log = LoggerFactory.getLogger(IssueServiceImpl.class);

    private final IssueRepository issueRepository;

    private final IssueMapper issueMapper;

    private final CommandExecuteService commandExecuteService;

    public IssueServiceImpl(IssueRepository issueRepository, IssueMapper issueMapper,
            CommandExecuteService commandExecuteService) {
        this.issueRepository = issueRepository;
        this.issueMapper = issueMapper;
        this.commandExecuteService = commandExecuteService;
    }

    /**
     * Save a issue.
     *
     * @param issueDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public IssueDTO save(IssueDTO issueDTO) {
        log.debug("Request to save Issue : {}", issueDTO);
        Issue issue = issueMapper.toEntity(issueDTO);
        issue = issueRepository.save(issue);
        return issueMapper.toDto(issue);
    }

    /**
     * Get all the issues.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<IssueDTO> findAll() {
        log.debug("Request to get all Issues");
        return issueRepository.findAll().stream().map(issueMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one issue by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public IssueDTO findOne(Long id) {
        log.debug("Request to get Issue : {}", id);
        Issue issue = issueRepository.findOne(id);
        return issueMapper.toDto(issue);
    }

    /**
     * Get all the issue for one subsystem.
     *
     * @param id the id of the subsystem
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public List<IssueDTO> findAllBySubsystemId(Long id) {
        log.debug("Request to get all Issues for Subsystem : {}", id);
        return issueRepository.findAllBySubsystemId(id).stream().map(issueMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Delete the issue by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Issue : {}", id);
        issueRepository.delete(id);
    }

    @Override
    public String executeCommand(Long id, String command) {
        log.debug("Request to execute command to Issue : {}", id);
        try {
            Issue issue = issueRepository.findOne(id);
            String response = commandExecuteService.executeImsCommand(issue.getSubsystemObject().getSubsystem(),
                    command);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            return new String("Error while executing command. Exception message: " + e.getMessage());
        }
    }
}
