import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { SubsystemObject } from './subsystem-object.model';
import { SubsystemObjectService } from './subsystem-object.service';

@Injectable()
export class SubsystemObjectPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private subsystemObjectService: SubsystemObjectService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.subsystemObjectService.find(id)
                    .subscribe((subsystemObjectResponse: HttpResponse<SubsystemObject>) => {
                        const subsystemObject: SubsystemObject = subsystemObjectResponse.body;
                        this.ngbModalRef = this.subsystemObjectModalRef(component, subsystemObject);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.subsystemObjectModalRef(component, new SubsystemObject());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    subsystemObjectModalRef(component: Component, subsystemObject: SubsystemObject): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.subsystemObject = subsystemObject;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
