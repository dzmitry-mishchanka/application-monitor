package com.mishchenko.appmonitor.service;

import com.mishchenko.appmonitor.service.dto.IssueDTO;
import java.util.List;

/**
 * Service Interface for managing Issue.
 */
public interface IssueService {

    /**
     * Save a issue.
     *
     * @param issueDTO the entity to save
     * @return the persisted entity
     */
    IssueDTO save(IssueDTO issueDTO);

    /**
     * Get all the issues.
     *
     * @return the list of entities
     */
    List<IssueDTO> findAll();

    /**
     * Get the "id" issue.
     *
     * @param id the id of the entity
     * @return the entity
     */
    IssueDTO findOne(Long id);

    /**
     * Get all the issues for one subsystem.
     *
     * @param id the id of the subsystem
     * @return the entity
     */
    List<IssueDTO> findAllBySubsystemId(Long id);

    /**
     * Delete the "id" issue.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Execute command to the "id" issue.
     *
     * @param id the id of the entity
     * @return the command response
     */
    String executeCommand(Long id, String command);
}
