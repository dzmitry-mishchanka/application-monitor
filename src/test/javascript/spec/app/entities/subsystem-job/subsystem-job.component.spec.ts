/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AppmonitorTestModule } from '../../../test.module';
import { SubsystemJobComponent } from '../../../../../../main/webapp/app/entities/subsystem-job/subsystem-job.component';
import { SubsystemJobService } from '../../../../../../main/webapp/app/entities/subsystem-job/subsystem-job.service';
import { SubsystemJob } from '../../../../../../main/webapp/app/entities/subsystem-job/subsystem-job.model';

describe('Component Tests', () => {

    describe('SubsystemJob Management Component', () => {
        let comp: SubsystemJobComponent;
        let fixture: ComponentFixture<SubsystemJobComponent>;
        let service: SubsystemJobService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [AppmonitorTestModule],
                declarations: [SubsystemJobComponent],
                providers: [
                    SubsystemJobService
                ]
            })
            .overrideTemplate(SubsystemJobComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SubsystemJobComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SubsystemJobService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new SubsystemJob(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.subsystemJobs[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
