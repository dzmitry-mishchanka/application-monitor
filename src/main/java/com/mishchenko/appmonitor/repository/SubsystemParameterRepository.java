package com.mishchenko.appmonitor.repository;

import java.util.List;

import com.mishchenko.appmonitor.domain.SubsystemParameter;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the SubsystemParameter entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubsystemParameterRepository extends JpaRepository<SubsystemParameter, Long> {

    @Query("select subsystemParameter from SubsystemParameter subsystemParameter where subsystemParameter.subsystem.id = :id")
    List<SubsystemParameter> findAllBySubsystemId(@Param("id") Long id);
}
