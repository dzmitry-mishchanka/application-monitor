import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { SubsystemParameterComponent } from './subsystem-parameter.component';
import { SubsystemParameterDetailComponent } from './subsystem-parameter-detail.component';
import { SubsystemParameterPopupComponent } from './subsystem-parameter-dialog.component';
import { SubsystemParameterDeletePopupComponent } from './subsystem-parameter-delete-dialog.component';

export const subsystemParameterRoute: Routes = [
    {
        path: 'subsystem-parameter',
        component: SubsystemParameterComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.subsystemParameter.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'subsystem-parameter/:id',
        component: SubsystemParameterDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.subsystemParameter.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const subsystemParameterPopupRoute: Routes = [
    {
        path: 'subsystem-parameter-new',
        component: SubsystemParameterPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.subsystemParameter.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'subsystem-parameter/:id/edit',
        component: SubsystemParameterPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.subsystemParameter.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'subsystem-parameter/:id/delete',
        component: SubsystemParameterDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.subsystemParameter.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
