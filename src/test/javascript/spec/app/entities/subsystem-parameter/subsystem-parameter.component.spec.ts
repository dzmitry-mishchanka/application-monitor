/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AppmonitorTestModule } from '../../../test.module';
import { SubsystemParameterComponent } from '../../../../../../main/webapp/app/entities/subsystem-parameter/subsystem-parameter.component';
import { SubsystemParameterService } from '../../../../../../main/webapp/app/entities/subsystem-parameter/subsystem-parameter.service';
import { SubsystemParameter } from '../../../../../../main/webapp/app/entities/subsystem-parameter/subsystem-parameter.model';

describe('Component Tests', () => {

    describe('SubsystemParameter Management Component', () => {
        let comp: SubsystemParameterComponent;
        let fixture: ComponentFixture<SubsystemParameterComponent>;
        let service: SubsystemParameterService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [AppmonitorTestModule],
                declarations: [SubsystemParameterComponent],
                providers: [
                    SubsystemParameterService
                ]
            })
            .overrideTemplate(SubsystemParameterComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SubsystemParameterComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SubsystemParameterService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new SubsystemParameter(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.subsystemParameters[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
