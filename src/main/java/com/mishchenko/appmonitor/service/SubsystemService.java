package com.mishchenko.appmonitor.service;

import java.util.List;

import com.mishchenko.appmonitor.service.dto.SubsystemDTO;

/**
 * Service Interface for managing Subsystem.
 */
public interface SubsystemService {

    /**
     * Save a subsystem.
     *
     * @param subsystemDTO the entity to save
     * @return the persisted entity
     */
    SubsystemDTO save(SubsystemDTO subsystemDTO);

    /**
     * Get all the subsystems.
     *
     * @return the list of entities
     */
    List<SubsystemDTO> findAll();

    /**
     * Get the "id" subsystem.
     *
     * @param id the id of the entity
     * @return the entity
     */
    SubsystemDTO findOne(Long id);

    /**
     * Delete the "id" subsystem.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Update issues in the "id" subsystem.
     *
     * @param id the id of the entity
     */
    void updateIssues(Long id);

    /**
     * Update information of the "id" subsystem.
     *
     * @param id the id of the entity
     */
    void updateInformation(Long id);
}
