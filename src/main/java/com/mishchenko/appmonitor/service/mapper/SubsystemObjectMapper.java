package com.mishchenko.appmonitor.service.mapper;

import com.mishchenko.appmonitor.domain.*;
import com.mishchenko.appmonitor.service.dto.SubsystemObjectDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SubsystemObject and its DTO SubsystemObjectDTO.
 */
@Mapper(componentModel = "spring", uses = {SubsystemMapper.class, ObjectTypeMapper.class})
public interface SubsystemObjectMapper extends EntityMapper<SubsystemObjectDTO, SubsystemObject> {

    @Mapping(source = "subsystem.id", target = "subsystemId")
    @Mapping(source = "objectType.id", target = "objectTypeId")
    SubsystemObjectDTO toDto(SubsystemObject subsystemObject);

    @Mapping(source = "subsystemId", target = "subsystem")
    @Mapping(source = "objectTypeId", target = "objectType")
    SubsystemObject toEntity(SubsystemObjectDTO subsystemObjectDTO);

    default SubsystemObject fromId(Long id) {
        if (id == null) {
            return null;
        }
        SubsystemObject subsystemObject = new SubsystemObject();
        subsystemObject.setId(id);
        return subsystemObject;
    }
}
