package com.mishchenko.appmonitor.zos.util.ims;

import java.io.Serializable;

public class ImsQueuedMessage implements Serializable {

	private static final long serialVersionUID = 1L;

	private String transactionName;

	private String psbName;

	private int messageCount;

	public void setTransactionName(String transactionName) {
		this.transactionName = transactionName;
	}

	public String getTransactionName() {
		return transactionName;
	}

	public void setPsbName(String psbName) {
		this.psbName = psbName;
	}

	public String getPsbName() {
		return psbName;
	}

	public void setMessageCount(int messageCount) {
		this.messageCount = messageCount;
	}

	public int getMessageCount() {
		return messageCount;
	}

	@Override
	public String toString() {
		return "ImsQueuedMessage{" + "transactionName='" + getTransactionName() + "', psbName='" + getPsbName()
				+ "', messageCount='" + getMessageCount() + "'}";
	}
}