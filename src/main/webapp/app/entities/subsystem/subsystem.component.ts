import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { Subsystem } from './subsystem.model';
import { SubsystemService } from './subsystem.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-subsystem',
    templateUrl: './subsystem.component.html'
})
export class SubsystemComponent implements OnInit, OnDestroy {
subsystems: Subsystem[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private subsystemService: SubsystemService,
        private jhiAlertService: JhiAlertService,
        private dataUtils: JhiDataUtils,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.subsystemService.query().subscribe(
            (res: HttpResponse<Subsystem[]>) => {
                this.subsystems = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInSubsystems();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Subsystem) {
        return item.id;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    registerChangeInSubsystems() {
        this.eventSubscriber = this.eventManager.subscribe('subsystemListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
