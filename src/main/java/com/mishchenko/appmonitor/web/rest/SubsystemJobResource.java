package com.mishchenko.appmonitor.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mishchenko.appmonitor.service.SubsystemJobService;
import com.mishchenko.appmonitor.web.rest.errors.BadRequestAlertException;
import com.mishchenko.appmonitor.web.rest.util.HeaderUtil;
import com.mishchenko.appmonitor.web.rest.util.PaginationUtil;
import com.mishchenko.appmonitor.service.dto.SubsystemJobDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SubsystemJob.
 */
@RestController
@RequestMapping("/api")
public class SubsystemJobResource {

    private final Logger log = LoggerFactory.getLogger(SubsystemJobResource.class);

    private static final String ENTITY_NAME = "subsystemJob";

    private final SubsystemJobService subsystemJobService;

    public SubsystemJobResource(SubsystemJobService subsystemJobService) {
        this.subsystemJobService = subsystemJobService;
    }

    /**
     * POST /subsystem-jobs : Create a new subsystemJob.
     *
     * @param subsystemJobDTO the subsystemJobDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new
     *         subsystemJobDTO, or with status 400 (Bad Request) if the subsystemJob
     *         has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/subsystem-jobs")
    @Timed
    public ResponseEntity<SubsystemJobDTO> createSubsystemJob(@RequestBody SubsystemJobDTO subsystemJobDTO)
            throws URISyntaxException {
        log.debug("REST request to save SubsystemJob : {}", subsystemJobDTO);
        if (subsystemJobDTO.getId() != null) {
            throw new BadRequestAlertException("A new subsystemJob cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SubsystemJobDTO result = subsystemJobService.save(subsystemJobDTO);
        return ResponseEntity.created(new URI("/api/subsystem-jobs/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT /subsystem-jobs : Updates an existing subsystemJob.
     *
     * @param subsystemJobDTO the subsystemJobDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated
     *         subsystemJobDTO, or with status 400 (Bad Request) if the
     *         subsystemJobDTO is not valid, or with status 500 (Internal Server
     *         Error) if the subsystemJobDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/subsystem-jobs")
    @Timed
    public ResponseEntity<SubsystemJobDTO> updateSubsystemJob(@RequestBody SubsystemJobDTO subsystemJobDTO)
            throws URISyntaxException {
        log.debug("REST request to update SubsystemJob : {}", subsystemJobDTO);
        if (subsystemJobDTO.getId() == null) {
            return createSubsystemJob(subsystemJobDTO);
        }
        SubsystemJobDTO result = subsystemJobService.save(subsystemJobDTO);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, subsystemJobDTO.getId().toString()))
                .body(result);
    }

    /**
     * GET /subsystem-jobs : get all the subsystemJobs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of subsystemJobs
     *         in body
     */
    @GetMapping("/subsystem-jobs")
    @Timed
    public ResponseEntity<List<SubsystemJobDTO>> getAllSubsystemJobs(Pageable pageable) {
        log.debug("REST request to get a page of SubsystemJobs");
        Page<SubsystemJobDTO> page = subsystemJobService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/subsystem-jobs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET /subsystem-jobs/:id : get the "id" subsystemJob.
     *
     * @param id the id of the subsystemJobDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the
     *         subsystemJobDTO, or with status 404 (Not Found)
     */
    @GetMapping("/subsystem-jobs/{id}")
    @Timed
    public ResponseEntity<SubsystemJobDTO> getSubsystemJob(@PathVariable Long id) {
        log.debug("REST request to get SubsystemJob : {}", id);
        SubsystemJobDTO subsystemJobDTO = subsystemJobService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(subsystemJobDTO));
    }

    @GetMapping("/subsystem-jobs/getBySubsystem/{id}")
    @Timed
    public List<SubsystemJobDTO> getSubsystemJobsBySubsystem(@PathVariable Long id) {
        log.debug("REST request to get SubsystemJob for Subsystem : {}", id);
        return subsystemJobService.findAllBySubsystemId(id);
    }

    /**
     * DELETE /subsystem-jobs/:id : delete the "id" subsystemJob.
     *
     * @param id the id of the subsystemJobDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/subsystem-jobs/{id}")
    @Timed
    public ResponseEntity<Void> deleteSubsystemJob(@PathVariable Long id) {
        log.debug("REST request to delete SubsystemJob : {}", id);
        subsystemJobService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
