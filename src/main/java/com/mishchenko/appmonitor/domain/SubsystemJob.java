package com.mishchenko.appmonitor.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import com.mishchenko.appmonitor.domain.enumeration.JesJobStatus;

import com.mishchenko.appmonitor.domain.enumeration.SubsystemJobType;

/**
 * A SubsystemJob.
 */
@Entity
@Table(name = "subsystem_job")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SubsystemJob implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private JesJobStatus status;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private SubsystemJobType type;

    @Column(name = "started_at")
    private ZonedDateTime startedAt;

    @Lob
    @Column(name = "log")
    private String log;

    @ManyToOne
    private Subsystem subsystem;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public SubsystemJob name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public SubsystemJob description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public JesJobStatus getStatus() {
        return status;
    }

    public SubsystemJob status(JesJobStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(JesJobStatus status) {
        this.status = status;
    }

    public SubsystemJobType getType() {
        return type;
    }

    public SubsystemJob type(SubsystemJobType type) {
        this.type = type;
        return this;
    }

    public void setType(SubsystemJobType type) {
        this.type = type;
    }

    public ZonedDateTime getStartedAt() {
        return startedAt;
    }

    public SubsystemJob startedAt(ZonedDateTime startedAt) {
        this.startedAt = startedAt;
        return this;
    }

    public void setStartedAt(ZonedDateTime startedAt) {
        this.startedAt = startedAt;
    }

    public String getLog() {
        return log;
    }

    public SubsystemJob log(String log) {
        this.log = log;
        return this;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public Subsystem getSubsystem() {
        return subsystem;
    }

    public SubsystemJob subsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
        return this;
    }

    public void setSubsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SubsystemJob subsystemJob = (SubsystemJob) o;
        if (subsystemJob.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), subsystemJob.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SubsystemJob{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", status='" + getStatus() + "'" +
            ", type='" + getType() + "'" +
            ", startedAt='" + getStartedAt() + "'" +
            ", log='" + getLog() + "'" +
            "}";
    }
}
