package com.mishchenko.appmonitor.web.rest;

import com.mishchenko.appmonitor.AppmonitorApp;

import com.mishchenko.appmonitor.domain.ObjectType;
import com.mishchenko.appmonitor.repository.ObjectTypeRepository;
import com.mishchenko.appmonitor.service.ObjectTypeService;
import com.mishchenko.appmonitor.service.dto.ObjectTypeDTO;
import com.mishchenko.appmonitor.service.mapper.ObjectTypeMapper;
import com.mishchenko.appmonitor.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.mishchenko.appmonitor.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ObjectTypeResource REST controller.
 *
 * @see ObjectTypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppmonitorApp.class)
public class ObjectTypeResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private ObjectTypeRepository objectTypeRepository;

    @Autowired
    private ObjectTypeMapper objectTypeMapper;

    @Autowired
    private ObjectTypeService objectTypeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restObjectTypeMockMvc;

    private ObjectType objectType;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ObjectTypeResource objectTypeResource = new ObjectTypeResource(objectTypeService);
        this.restObjectTypeMockMvc = MockMvcBuilders.standaloneSetup(objectTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ObjectType createEntity(EntityManager em) {
        ObjectType objectType = new ObjectType()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION);
        return objectType;
    }

    @Before
    public void initTest() {
        objectType = createEntity(em);
    }

    @Test
    @Transactional
    public void createObjectType() throws Exception {
        int databaseSizeBeforeCreate = objectTypeRepository.findAll().size();

        // Create the ObjectType
        ObjectTypeDTO objectTypeDTO = objectTypeMapper.toDto(objectType);
        restObjectTypeMockMvc.perform(post("/api/object-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(objectTypeDTO)))
            .andExpect(status().isCreated());

        // Validate the ObjectType in the database
        List<ObjectType> objectTypeList = objectTypeRepository.findAll();
        assertThat(objectTypeList).hasSize(databaseSizeBeforeCreate + 1);
        ObjectType testObjectType = objectTypeList.get(objectTypeList.size() - 1);
        assertThat(testObjectType.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testObjectType.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createObjectTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = objectTypeRepository.findAll().size();

        // Create the ObjectType with an existing ID
        objectType.setId(1L);
        ObjectTypeDTO objectTypeDTO = objectTypeMapper.toDto(objectType);

        // An entity with an existing ID cannot be created, so this API call must fail
        restObjectTypeMockMvc.perform(post("/api/object-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(objectTypeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ObjectType in the database
        List<ObjectType> objectTypeList = objectTypeRepository.findAll();
        assertThat(objectTypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllObjectTypes() throws Exception {
        // Initialize the database
        objectTypeRepository.saveAndFlush(objectType);

        // Get all the objectTypeList
        restObjectTypeMockMvc.perform(get("/api/object-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(objectType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getObjectType() throws Exception {
        // Initialize the database
        objectTypeRepository.saveAndFlush(objectType);

        // Get the objectType
        restObjectTypeMockMvc.perform(get("/api/object-types/{id}", objectType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(objectType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingObjectType() throws Exception {
        // Get the objectType
        restObjectTypeMockMvc.perform(get("/api/object-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateObjectType() throws Exception {
        // Initialize the database
        objectTypeRepository.saveAndFlush(objectType);
        int databaseSizeBeforeUpdate = objectTypeRepository.findAll().size();

        // Update the objectType
        ObjectType updatedObjectType = objectTypeRepository.findOne(objectType.getId());
        // Disconnect from session so that the updates on updatedObjectType are not directly saved in db
        em.detach(updatedObjectType);
        updatedObjectType
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);
        ObjectTypeDTO objectTypeDTO = objectTypeMapper.toDto(updatedObjectType);

        restObjectTypeMockMvc.perform(put("/api/object-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(objectTypeDTO)))
            .andExpect(status().isOk());

        // Validate the ObjectType in the database
        List<ObjectType> objectTypeList = objectTypeRepository.findAll();
        assertThat(objectTypeList).hasSize(databaseSizeBeforeUpdate);
        ObjectType testObjectType = objectTypeList.get(objectTypeList.size() - 1);
        assertThat(testObjectType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testObjectType.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingObjectType() throws Exception {
        int databaseSizeBeforeUpdate = objectTypeRepository.findAll().size();

        // Create the ObjectType
        ObjectTypeDTO objectTypeDTO = objectTypeMapper.toDto(objectType);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restObjectTypeMockMvc.perform(put("/api/object-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(objectTypeDTO)))
            .andExpect(status().isCreated());

        // Validate the ObjectType in the database
        List<ObjectType> objectTypeList = objectTypeRepository.findAll();
        assertThat(objectTypeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteObjectType() throws Exception {
        // Initialize the database
        objectTypeRepository.saveAndFlush(objectType);
        int databaseSizeBeforeDelete = objectTypeRepository.findAll().size();

        // Get the objectType
        restObjectTypeMockMvc.perform(delete("/api/object-types/{id}", objectType.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ObjectType> objectTypeList = objectTypeRepository.findAll();
        assertThat(objectTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ObjectType.class);
        ObjectType objectType1 = new ObjectType();
        objectType1.setId(1L);
        ObjectType objectType2 = new ObjectType();
        objectType2.setId(objectType1.getId());
        assertThat(objectType1).isEqualTo(objectType2);
        objectType2.setId(2L);
        assertThat(objectType1).isNotEqualTo(objectType2);
        objectType1.setId(null);
        assertThat(objectType1).isNotEqualTo(objectType2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ObjectTypeDTO.class);
        ObjectTypeDTO objectTypeDTO1 = new ObjectTypeDTO();
        objectTypeDTO1.setId(1L);
        ObjectTypeDTO objectTypeDTO2 = new ObjectTypeDTO();
        assertThat(objectTypeDTO1).isNotEqualTo(objectTypeDTO2);
        objectTypeDTO2.setId(objectTypeDTO1.getId());
        assertThat(objectTypeDTO1).isEqualTo(objectTypeDTO2);
        objectTypeDTO2.setId(2L);
        assertThat(objectTypeDTO1).isNotEqualTo(objectTypeDTO2);
        objectTypeDTO1.setId(null);
        assertThat(objectTypeDTO1).isNotEqualTo(objectTypeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(objectTypeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(objectTypeMapper.fromId(null)).isNull();
    }
}
