package com.mishchenko.appmonitor.zos.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import com.mishchenko.appmonitor.domain.Issue;
import com.mishchenko.appmonitor.domain.User;
import com.mishchenko.appmonitor.repository.UserRepository;
import com.mishchenko.appmonitor.service.MailService;
import com.mishchenko.appmonitor.zos.service.IssueNotificationService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import io.github.jhipster.config.JHipsterProperties;

@Service
@Transactional
public class IssueNotificationServiceImpl implements IssueNotificationService {

    private static final Logger log = LoggerFactory.getLogger(IssueNotificationServiceImpl.class);

    private static final String USER = "user";

    private static final String ISSUE = "issue";

    private static final String BASE_URL = "baseUrl";

    private final JHipsterProperties jHipsterProperties;

    private final MessageSource messageSource;

    private final MailService mailService;

    private final SpringTemplateEngine templateEngine;

    private final UserRepository userRepository;

    public IssueNotificationServiceImpl(JHipsterProperties jHipsterProperties, MessageSource messageSource,
            MailService mailService, SpringTemplateEngine templateEngine, UserRepository userRepository) {
        this.jHipsterProperties = jHipsterProperties;
        this.messageSource = messageSource;
        this.mailService = mailService;
        this.templateEngine = templateEngine;
        this.userRepository = userRepository;
    }

    @Override
    public void notify(Issue issue) throws Exception {
        List<User> users = userRepository.findAll();
        for (User user : users) {
            log.debug("Sending issue #{} notification email to '{}'", issue.getId(), user.getEmail());
            sendEmailFromTemplate(user, issue, "issueNotificationEmail", "email.issue.notification.title");
        }
    }

    @Override
    public void notify(Collection<Issue> issues) throws Exception {
        for (Issue issue : issues) {
            notify(issue);
        }
    }

    @Async
    public void sendEmailFromTemplate(User user, Issue issue, String templateName, String titleKey) {
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(ISSUE, issue);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, new Object[] { issue.getId() }, locale);
        mailService.sendEmail(user.getEmail(), subject, content, false, true);
    }
}
