import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SubsystemJob } from '../../entities/subsystem-job';

@Component({
    selector: 'jhi-subsystem-job-log-modal',
    templateUrl: './subsystem-job-log-modal.component.html',
    styleUrls: [
        'subsystem-job-log-modal.css'
    ]
})
export class SubsystemJobLogModalComponent implements OnInit {

    subsystemJob: SubsystemJob;
    logLines: string[];

    constructor(
        public activeModal: NgbActiveModal,
    ) { }

    ngOnInit() {
        this.logLines = this.subsystemJob.log.split('\n');
    }

    close() {
        this.activeModal.dismiss('cancel');
    }
}
