package com.mishchenko.appmonitor.web.rest;

import com.mishchenko.appmonitor.AppmonitorApp;

import com.mishchenko.appmonitor.domain.SubsystemParameter;
import com.mishchenko.appmonitor.repository.SubsystemParameterRepository;
import com.mishchenko.appmonitor.service.SubsystemParameterService;
import com.mishchenko.appmonitor.service.dto.SubsystemParameterDTO;
import com.mishchenko.appmonitor.service.mapper.SubsystemParameterMapper;
import com.mishchenko.appmonitor.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.mishchenko.appmonitor.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SubsystemParameterResource REST controller.
 *
 * @see SubsystemParameterResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppmonitorApp.class)
public class SubsystemParameterResourceIntTest {

    private static final String DEFAULT_PARAMETER_KEY = "AAAAAAAAAA";
    private static final String UPDATED_PARAMETER_KEY = "BBBBBBBBBB";

    private static final String DEFAULT_PARAMETER_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_PARAMETER_VALUE = "BBBBBBBBBB";

    @Autowired
    private SubsystemParameterRepository subsystemParameterRepository;

    @Autowired
    private SubsystemParameterMapper subsystemParameterMapper;

    @Autowired
    private SubsystemParameterService subsystemParameterService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSubsystemParameterMockMvc;

    private SubsystemParameter subsystemParameter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SubsystemParameterResource subsystemParameterResource = new SubsystemParameterResource(subsystemParameterService);
        this.restSubsystemParameterMockMvc = MockMvcBuilders.standaloneSetup(subsystemParameterResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubsystemParameter createEntity(EntityManager em) {
        SubsystemParameter subsystemParameter = new SubsystemParameter()
            .parameterKey(DEFAULT_PARAMETER_KEY)
            .parameterValue(DEFAULT_PARAMETER_VALUE);
        return subsystemParameter;
    }

    @Before
    public void initTest() {
        subsystemParameter = createEntity(em);
    }

    @Test
    @Transactional
    public void createSubsystemParameter() throws Exception {
        int databaseSizeBeforeCreate = subsystemParameterRepository.findAll().size();

        // Create the SubsystemParameter
        SubsystemParameterDTO subsystemParameterDTO = subsystemParameterMapper.toDto(subsystemParameter);
        restSubsystemParameterMockMvc.perform(post("/api/subsystem-parameters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subsystemParameterDTO)))
            .andExpect(status().isCreated());

        // Validate the SubsystemParameter in the database
        List<SubsystemParameter> subsystemParameterList = subsystemParameterRepository.findAll();
        assertThat(subsystemParameterList).hasSize(databaseSizeBeforeCreate + 1);
        SubsystemParameter testSubsystemParameter = subsystemParameterList.get(subsystemParameterList.size() - 1);
        assertThat(testSubsystemParameter.getParameterKey()).isEqualTo(DEFAULT_PARAMETER_KEY);
        assertThat(testSubsystemParameter.getParameterValue()).isEqualTo(DEFAULT_PARAMETER_VALUE);
    }

    @Test
    @Transactional
    public void createSubsystemParameterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = subsystemParameterRepository.findAll().size();

        // Create the SubsystemParameter with an existing ID
        subsystemParameter.setId(1L);
        SubsystemParameterDTO subsystemParameterDTO = subsystemParameterMapper.toDto(subsystemParameter);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSubsystemParameterMockMvc.perform(post("/api/subsystem-parameters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subsystemParameterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SubsystemParameter in the database
        List<SubsystemParameter> subsystemParameterList = subsystemParameterRepository.findAll();
        assertThat(subsystemParameterList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSubsystemParameters() throws Exception {
        // Initialize the database
        subsystemParameterRepository.saveAndFlush(subsystemParameter);

        // Get all the subsystemParameterList
        restSubsystemParameterMockMvc.perform(get("/api/subsystem-parameters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(subsystemParameter.getId().intValue())))
            .andExpect(jsonPath("$.[*].parameterKey").value(hasItem(DEFAULT_PARAMETER_KEY.toString())))
            .andExpect(jsonPath("$.[*].parameterValue").value(hasItem(DEFAULT_PARAMETER_VALUE.toString())));
    }

    @Test
    @Transactional
    public void getSubsystemParameter() throws Exception {
        // Initialize the database
        subsystemParameterRepository.saveAndFlush(subsystemParameter);

        // Get the subsystemParameter
        restSubsystemParameterMockMvc.perform(get("/api/subsystem-parameters/{id}", subsystemParameter.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(subsystemParameter.getId().intValue()))
            .andExpect(jsonPath("$.parameterKey").value(DEFAULT_PARAMETER_KEY.toString()))
            .andExpect(jsonPath("$.parameterValue").value(DEFAULT_PARAMETER_VALUE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSubsystemParameter() throws Exception {
        // Get the subsystemParameter
        restSubsystemParameterMockMvc.perform(get("/api/subsystem-parameters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSubsystemParameter() throws Exception {
        // Initialize the database
        subsystemParameterRepository.saveAndFlush(subsystemParameter);
        int databaseSizeBeforeUpdate = subsystemParameterRepository.findAll().size();

        // Update the subsystemParameter
        SubsystemParameter updatedSubsystemParameter = subsystemParameterRepository.findOne(subsystemParameter.getId());
        // Disconnect from session so that the updates on updatedSubsystemParameter are not directly saved in db
        em.detach(updatedSubsystemParameter);
        updatedSubsystemParameter
            .parameterKey(UPDATED_PARAMETER_KEY)
            .parameterValue(UPDATED_PARAMETER_VALUE);
        SubsystemParameterDTO subsystemParameterDTO = subsystemParameterMapper.toDto(updatedSubsystemParameter);

        restSubsystemParameterMockMvc.perform(put("/api/subsystem-parameters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subsystemParameterDTO)))
            .andExpect(status().isOk());

        // Validate the SubsystemParameter in the database
        List<SubsystemParameter> subsystemParameterList = subsystemParameterRepository.findAll();
        assertThat(subsystemParameterList).hasSize(databaseSizeBeforeUpdate);
        SubsystemParameter testSubsystemParameter = subsystemParameterList.get(subsystemParameterList.size() - 1);
        assertThat(testSubsystemParameter.getParameterKey()).isEqualTo(UPDATED_PARAMETER_KEY);
        assertThat(testSubsystemParameter.getParameterValue()).isEqualTo(UPDATED_PARAMETER_VALUE);
    }

    @Test
    @Transactional
    public void updateNonExistingSubsystemParameter() throws Exception {
        int databaseSizeBeforeUpdate = subsystemParameterRepository.findAll().size();

        // Create the SubsystemParameter
        SubsystemParameterDTO subsystemParameterDTO = subsystemParameterMapper.toDto(subsystemParameter);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSubsystemParameterMockMvc.perform(put("/api/subsystem-parameters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subsystemParameterDTO)))
            .andExpect(status().isCreated());

        // Validate the SubsystemParameter in the database
        List<SubsystemParameter> subsystemParameterList = subsystemParameterRepository.findAll();
        assertThat(subsystemParameterList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSubsystemParameter() throws Exception {
        // Initialize the database
        subsystemParameterRepository.saveAndFlush(subsystemParameter);
        int databaseSizeBeforeDelete = subsystemParameterRepository.findAll().size();

        // Get the subsystemParameter
        restSubsystemParameterMockMvc.perform(delete("/api/subsystem-parameters/{id}", subsystemParameter.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SubsystemParameter> subsystemParameterList = subsystemParameterRepository.findAll();
        assertThat(subsystemParameterList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubsystemParameter.class);
        SubsystemParameter subsystemParameter1 = new SubsystemParameter();
        subsystemParameter1.setId(1L);
        SubsystemParameter subsystemParameter2 = new SubsystemParameter();
        subsystemParameter2.setId(subsystemParameter1.getId());
        assertThat(subsystemParameter1).isEqualTo(subsystemParameter2);
        subsystemParameter2.setId(2L);
        assertThat(subsystemParameter1).isNotEqualTo(subsystemParameter2);
        subsystemParameter1.setId(null);
        assertThat(subsystemParameter1).isNotEqualTo(subsystemParameter2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubsystemParameterDTO.class);
        SubsystemParameterDTO subsystemParameterDTO1 = new SubsystemParameterDTO();
        subsystemParameterDTO1.setId(1L);
        SubsystemParameterDTO subsystemParameterDTO2 = new SubsystemParameterDTO();
        assertThat(subsystemParameterDTO1).isNotEqualTo(subsystemParameterDTO2);
        subsystemParameterDTO2.setId(subsystemParameterDTO1.getId());
        assertThat(subsystemParameterDTO1).isEqualTo(subsystemParameterDTO2);
        subsystemParameterDTO2.setId(2L);
        assertThat(subsystemParameterDTO1).isNotEqualTo(subsystemParameterDTO2);
        subsystemParameterDTO1.setId(null);
        assertThat(subsystemParameterDTO1).isNotEqualTo(subsystemParameterDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(subsystemParameterMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(subsystemParameterMapper.fromId(null)).isNull();
    }
}
