import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppmonitorSubsystemModule } from './subsystem/subsystem.module';
import { AppmonitorSubsystemParameterModule } from './subsystem-parameter/subsystem-parameter.module';
import { AppmonitorObjectTypeModule } from './object-type/object-type.module';
import { AppmonitorObjectStatusModule } from './object-status/object-status.module';
import { AppmonitorIssueModule } from './issue/issue.module';
import { AppmonitorSubsystemJobModule } from './subsystem-job/subsystem-job.module';
import { AppmonitorSubsystemObjectModule } from './subsystem-object/subsystem-object.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        AppmonitorSubsystemModule,
        AppmonitorSubsystemParameterModule,
        AppmonitorObjectTypeModule,
        AppmonitorObjectStatusModule,
        AppmonitorIssueModule,
        AppmonitorSubsystemJobModule,
        AppmonitorSubsystemObjectModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppmonitorEntityModule {}
