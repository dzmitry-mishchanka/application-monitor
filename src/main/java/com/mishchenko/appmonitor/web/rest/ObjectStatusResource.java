package com.mishchenko.appmonitor.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mishchenko.appmonitor.service.ObjectStatusService;
import com.mishchenko.appmonitor.web.rest.errors.BadRequestAlertException;
import com.mishchenko.appmonitor.web.rest.util.HeaderUtil;
import com.mishchenko.appmonitor.service.dto.ObjectStatusDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ObjectStatus.
 */
@RestController
@RequestMapping("/api")
public class ObjectStatusResource {

    private final Logger log = LoggerFactory.getLogger(ObjectStatusResource.class);

    private static final String ENTITY_NAME = "objectStatus";

    private final ObjectStatusService objectStatusService;

    public ObjectStatusResource(ObjectStatusService objectStatusService) {
        this.objectStatusService = objectStatusService;
    }

    /**
     * POST  /object-statuses : Create a new objectStatus.
     *
     * @param objectStatusDTO the objectStatusDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new objectStatusDTO, or with status 400 (Bad Request) if the objectStatus has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/object-statuses")
    @Timed
    public ResponseEntity<ObjectStatusDTO> createObjectStatus(@RequestBody ObjectStatusDTO objectStatusDTO) throws URISyntaxException {
        log.debug("REST request to save ObjectStatus : {}", objectStatusDTO);
        if (objectStatusDTO.getId() != null) {
            throw new BadRequestAlertException("A new objectStatus cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ObjectStatusDTO result = objectStatusService.save(objectStatusDTO);
        return ResponseEntity.created(new URI("/api/object-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /object-statuses : Updates an existing objectStatus.
     *
     * @param objectStatusDTO the objectStatusDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated objectStatusDTO,
     * or with status 400 (Bad Request) if the objectStatusDTO is not valid,
     * or with status 500 (Internal Server Error) if the objectStatusDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/object-statuses")
    @Timed
    public ResponseEntity<ObjectStatusDTO> updateObjectStatus(@RequestBody ObjectStatusDTO objectStatusDTO) throws URISyntaxException {
        log.debug("REST request to update ObjectStatus : {}", objectStatusDTO);
        if (objectStatusDTO.getId() == null) {
            return createObjectStatus(objectStatusDTO);
        }
        ObjectStatusDTO result = objectStatusService.save(objectStatusDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, objectStatusDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /object-statuses : get all the objectStatuses.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of objectStatuses in body
     */
    @GetMapping("/object-statuses")
    @Timed
    public List<ObjectStatusDTO> getAllObjectStatuses() {
        log.debug("REST request to get all ObjectStatuses");
        return objectStatusService.findAll();
        }

    /**
     * GET  /object-statuses/:id : get the "id" objectStatus.
     *
     * @param id the id of the objectStatusDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the objectStatusDTO, or with status 404 (Not Found)
     */
    @GetMapping("/object-statuses/{id}")
    @Timed
    public ResponseEntity<ObjectStatusDTO> getObjectStatus(@PathVariable Long id) {
        log.debug("REST request to get ObjectStatus : {}", id);
        ObjectStatusDTO objectStatusDTO = objectStatusService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(objectStatusDTO));
    }

    /**
     * DELETE  /object-statuses/:id : delete the "id" objectStatus.
     *
     * @param id the id of the objectStatusDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/object-statuses/{id}")
    @Timed
    public ResponseEntity<Void> deleteObjectStatus(@PathVariable Long id) {
        log.debug("REST request to delete ObjectStatus : {}", id);
        objectStatusService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
