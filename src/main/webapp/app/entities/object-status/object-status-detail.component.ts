import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ObjectStatus } from './object-status.model';
import { ObjectStatusService } from './object-status.service';

@Component({
    selector: 'jhi-object-status-detail',
    templateUrl: './object-status-detail.component.html'
})
export class ObjectStatusDetailComponent implements OnInit, OnDestroy {

    objectStatus: ObjectStatus;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private objectStatusService: ObjectStatusService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInObjectStatuses();
    }

    load(id) {
        this.objectStatusService.find(id)
            .subscribe((objectStatusResponse: HttpResponse<ObjectStatus>) => {
                this.objectStatus = objectStatusResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInObjectStatuses() {
        this.eventSubscriber = this.eventManager.subscribe(
            'objectStatusListModification',
            (response) => this.load(this.objectStatus.id)
        );
    }
}
