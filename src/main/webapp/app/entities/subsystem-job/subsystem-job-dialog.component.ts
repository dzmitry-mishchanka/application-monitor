import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { SubsystemJob } from './subsystem-job.model';
import { SubsystemJobPopupService } from './subsystem-job-popup.service';
import { SubsystemJobService } from './subsystem-job.service';
import { Subsystem, SubsystemService } from '../subsystem';

@Component({
    selector: 'jhi-subsystem-job-dialog',
    templateUrl: './subsystem-job-dialog.component.html'
})
export class SubsystemJobDialogComponent implements OnInit {

    subsystemJob: SubsystemJob;
    isSaving: boolean;

    subsystems: Subsystem[];

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private jhiAlertService: JhiAlertService,
        private subsystemJobService: SubsystemJobService,
        private subsystemService: SubsystemService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.subsystemService.query()
            .subscribe((res: HttpResponse<Subsystem[]>) => { this.subsystems = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.subsystemJob.id !== undefined) {
            this.subscribeToSaveResponse(
                this.subsystemJobService.update(this.subsystemJob));
        } else {
            this.subscribeToSaveResponse(
                this.subsystemJobService.create(this.subsystemJob));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<SubsystemJob>>) {
        result.subscribe((res: HttpResponse<SubsystemJob>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: SubsystemJob) {
        this.eventManager.broadcast({ name: 'subsystemJobListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackSubsystemById(index: number, item: Subsystem) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-subsystem-job-popup',
    template: ''
})
export class SubsystemJobPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private subsystemJobPopupService: SubsystemJobPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.subsystemJobPopupService
                    .open(SubsystemJobDialogComponent as Component, params['id']);
            } else {
                this.subsystemJobPopupService
                    .open(SubsystemJobDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
