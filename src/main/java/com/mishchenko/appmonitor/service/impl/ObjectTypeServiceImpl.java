package com.mishchenko.appmonitor.service.impl;

import com.mishchenko.appmonitor.service.ObjectTypeService;
import com.mishchenko.appmonitor.domain.ObjectType;
import com.mishchenko.appmonitor.repository.ObjectTypeRepository;
import com.mishchenko.appmonitor.service.dto.ObjectTypeDTO;
import com.mishchenko.appmonitor.service.mapper.ObjectTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing ObjectType.
 */
@Service
@Transactional
public class ObjectTypeServiceImpl implements ObjectTypeService {

    private final Logger log = LoggerFactory.getLogger(ObjectTypeServiceImpl.class);

    private final ObjectTypeRepository objectTypeRepository;

    private final ObjectTypeMapper objectTypeMapper;

    public ObjectTypeServiceImpl(ObjectTypeRepository objectTypeRepository, ObjectTypeMapper objectTypeMapper) {
        this.objectTypeRepository = objectTypeRepository;
        this.objectTypeMapper = objectTypeMapper;
    }

    /**
     * Save a objectType.
     *
     * @param objectTypeDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ObjectTypeDTO save(ObjectTypeDTO objectTypeDTO) {
        log.debug("Request to save ObjectType : {}", objectTypeDTO);
        ObjectType objectType = objectTypeMapper.toEntity(objectTypeDTO);
        objectType = objectTypeRepository.save(objectType);
        return objectTypeMapper.toDto(objectType);
    }

    /**
     * Get all the objectTypes.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<ObjectTypeDTO> findAll() {
        log.debug("Request to get all ObjectTypes");
        return objectTypeRepository.findAll().stream()
            .map(objectTypeMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one objectType by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ObjectTypeDTO findOne(Long id) {
        log.debug("Request to get ObjectType : {}", id);
        ObjectType objectType = objectTypeRepository.findOne(id);
        return objectTypeMapper.toDto(objectType);
    }

    /**
     * Delete the objectType by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ObjectType : {}", id);
        objectTypeRepository.delete(id);
    }
}
