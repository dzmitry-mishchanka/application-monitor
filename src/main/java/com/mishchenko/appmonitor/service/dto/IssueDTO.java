package com.mishchenko.appmonitor.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DTO for the Issue entity.
 */
public class IssueDTO implements Serializable {

    private Long id;

    private ZonedDateTime occuredAt;

    private ZonedDateTime resolvedAt;

    private Long objectStatusId;

    private Long subsystemObjectId;

    private String subsystemObjectName;

    private String subsystemObjectDescription;

    private String objectTypeDescription;

    private String objectStatusName;

    private String objectStatusDescription;

    private Long assignedUserId;

    private String assignedUserLogin;

    private String assignedUserFirstName;

    private String assignedUserLastName;

    private String assignedUserEmail;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getOccuredAt() {
        return occuredAt;
    }

    public void setOccuredAt(ZonedDateTime occuredAt) {
        this.occuredAt = occuredAt;
    }

    public ZonedDateTime getResolvedAt() {
        return resolvedAt;
    }

    public void setResolvedAt(ZonedDateTime resolvedAt) {
        this.resolvedAt = resolvedAt;
    }

    public Long getSubsystemObjectId() {
        return subsystemObjectId;
    }

    public void setSubsystemObjectId(Long subsystemObjectId) {
        this.subsystemObjectId = subsystemObjectId;
    }

    public String getSubsystemObjectName() {
        return subsystemObjectName;
    }

    public void setSubsystemObjectName(String subsystemObjectName) {
        this.subsystemObjectName = subsystemObjectName;
    }

    public String getSubsystemObjectDescription() {
        return subsystemObjectDescription;
    }

    public void setSubsystemObjectDescription(String subsystemObjectDescription) {
        this.subsystemObjectDescription = subsystemObjectDescription;
    }

    public String getObjectTypeDescription() {
        return objectTypeDescription;
    }

    public void setObjectTypeDescription(String objectTypeDescription) {
        this.objectTypeDescription = objectTypeDescription;
    }

    public Long getObjectStatusId() {
        return objectStatusId;
    }

    public void setObjectStatusId(Long objectStatusId) {
        this.objectStatusId = objectStatusId;
    }

    public String getObjectStatusName() {
        return objectStatusName;
    }

    public void setObjectStatusName(String objectStatusName) {
        this.objectStatusName = objectStatusName;
    }

    public String getObjectStatusDescription() {
        return objectStatusDescription;
    }

    public void setObjectStatusDescription(String objectStatusDescription) {
        this.objectStatusDescription = objectStatusDescription;
    }

    public Long getAssignedUserId() {
        return assignedUserId;
    }

    public void setAssignedUserId(Long userId) {
        this.assignedUserId = userId;
    }

    public String getAssignedUserLogin() {
        return assignedUserLogin;
    }

    public void setAssignedUserLogin(String userLogin) {
        this.assignedUserLogin = userLogin;
    }

    public String getAssignedUserFirstName() {
        return assignedUserFirstName;
    }

    public void setAssignedUserFirstName(String assignedUserFirstName) {
        this.assignedUserFirstName = assignedUserFirstName;
    }

    public String getAssignedUserLastName() {
        return assignedUserLastName;
    }

    public void setAssignedUserLastName(String assignedUserLastName) {
        this.assignedUserLastName = assignedUserLastName;
    }

    public String getAssignedUserEmail() {
        return assignedUserEmail;
    }

    public void setAssignedUserEmail(String assignedUserEmail) {
        this.assignedUserEmail = assignedUserEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        IssueDTO issueDTO = (IssueDTO) o;
        if (issueDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), issueDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "IssueDTO{" + "id=" + getId() + ", occuredAt='" + getOccuredAt() + "'" + ", resolvedAt='"
                + getResolvedAt() + "'" + "}";
    }
}
