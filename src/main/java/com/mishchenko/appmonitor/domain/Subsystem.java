package com.mishchenko.appmonitor.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mishchenko.appmonitor.domain.enumeration.SubsystemType;
import com.vladmihalcea.hibernate.type.array.IntArrayType;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonNodeBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonNodeStringType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

/**
 * A Subsystem.
 */
@Entity
@Table(name = "subsystem")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@TypeDefs({ @TypeDef(name = "string-array", typeClass = StringArrayType.class),
        @TypeDef(name = "int-array", typeClass = IntArrayType.class),
        @TypeDef(name = "json", typeClass = JsonStringType.class),
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class),
        @TypeDef(name = "jsonb-node", typeClass = JsonNodeBinaryType.class),
        @TypeDef(name = "json-node", typeClass = JsonNodeStringType.class), })
public class Subsystem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "subsystem_type")
    private SubsystemType subsystemType;

    @Column(name = "refresh_rate")
    private Integer refreshRate;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Type(type = "json")
    @Column(name = "information", columnDefinition = "json")
    private SubsystemInformation information;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Subsystem name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SubsystemType getSubsystemType() {
        return subsystemType;
    }

    public Subsystem subsystemType(SubsystemType subsystemType) {
        this.subsystemType = subsystemType;
        return this;
    }

    public void setSubsystemType(SubsystemType subsystemType) {
        this.subsystemType = subsystemType;
    }

    public Integer getRefreshRate() {
        return refreshRate;
    }

    public Subsystem refreshRate(Integer refreshRate) {
        this.refreshRate = refreshRate;
        return this;
    }

    public void setRefreshRate(Integer refreshRate) {
        this.refreshRate = refreshRate;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public Subsystem updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public SubsystemInformation getInformation() {
        return information;
    }

    public Subsystem information(SubsystemInformation information) {
        this.information = information;
        return this;
    }

    public void setInformation(SubsystemInformation information) {
        this.information = information;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Subsystem subsystem = (Subsystem) o;
        if (subsystem.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), subsystem.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Subsystem{" + "id=" + getId() + ", name='" + getName() + "'" + ", subsystemType='" + getSubsystemType()
                + "'" + ", refreshRate=" + getRefreshRate() + ", updatedAt='" + getUpdatedAt() + "'" + ", information='"
                + getInformation() + "'" + "}";
    }
}
