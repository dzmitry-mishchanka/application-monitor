import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subsystem } from '../../entities/subsystem/subsystem.model';
import { Principal } from '../../shared/auth/principal.service';

@Component({
    selector: 'jhi-subsystem-card',
    templateUrl: './subsystem-card.component.html',
    styleUrls: [
        '../home.css'
    ]
})

export class SubsystemCardComponent implements OnInit, OnDestroy {

    @Input() subsystem: Subsystem;

    constructor(
        private principal: Principal,
        private modalService: NgbModal,
    ) {
    }

    ngOnInit() {
    }

    ngOnDestroy() {
    }

    getSubsystemUpdateDate() {
        return (new Date(this.subsystem.updatedAt.toString().substring(0, 19)).getTime());
    }
}
