package com.mishchenko.appmonitor.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import com.mishchenko.appmonitor.domain.Subsystem;
import com.mishchenko.appmonitor.repository.SubsystemRepository;
import com.mishchenko.appmonitor.service.SubsystemService;
import com.mishchenko.appmonitor.service.dto.SubsystemDTO;
import com.mishchenko.appmonitor.service.mapper.SubsystemMapper;
import com.mishchenko.appmonitor.zos.service.ConnectionFactoryService;
import com.mishchenko.appmonitor.zos.service.SubsystemUpdateService;
import com.mishchenko.appmonitor.zos.util.ZConnectionParameters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Subsystem.
 */
@Service
@Transactional
public class SubsystemServiceImpl implements SubsystemService {

    private final Logger log = LoggerFactory.getLogger(SubsystemServiceImpl.class);

    private final SubsystemRepository subsystemRepository;

    private final SubsystemMapper subsystemMapper;

    private final SubsystemUpdateService subsystemUpdateService;

    private final ConnectionFactoryService connectionFactoryService;

    public SubsystemServiceImpl(SubsystemRepository subsystemRepository, SubsystemMapper subsystemMapper,
            SubsystemUpdateService subsystemUpdateService, ConnectionFactoryService connectionFactoryService) {
        this.subsystemRepository = subsystemRepository;
        this.subsystemMapper = subsystemMapper;
        this.subsystemUpdateService = subsystemUpdateService;
        this.connectionFactoryService = connectionFactoryService;
    }

    /**
     * Save a subsystem.
     *
     * @param subsystemDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SubsystemDTO save(SubsystemDTO subsystemDTO) {
        log.debug("Request to save Subsystem : {}", subsystemDTO);
        Subsystem subsystem = subsystemMapper.toEntity(subsystemDTO);
        subsystem = subsystemRepository.save(subsystem);
        return subsystemMapper.toDto(subsystem);
    }

    /**
     * Get all the subsystems.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<SubsystemDTO> findAll() {
        log.debug("Request to get all Subsystems");
        return subsystemRepository.findAll().stream().map(subsystem -> {
            SubsystemDTO subsystemDTO = subsystemMapper.toDto(subsystem);
            ZConnectionParameters connectionParameters = connectionFactoryService.getParametersBySubsystem(subsystem);
            subsystemDTO.setConnectionParameters(connectionParameters);
            return subsystemDTO;
        }).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one subsystem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public SubsystemDTO findOne(Long id) {
        log.debug("Request to get Subsystem : {}", id);
        Subsystem subsystem = subsystemRepository.findOne(id);
        SubsystemDTO subsystemDTO = subsystemMapper.toDto(subsystem);
        ZConnectionParameters connectionParameters = connectionFactoryService.getParametersBySubsystem(subsystem);
        subsystemDTO.setConnectionParameters(connectionParameters);
        return subsystemDTO;
    }

    /**
     * Delete the subsystem by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Subsystem : {}", id);
        subsystemRepository.delete(id);
    }

    /**
     * Update issues in the "id" subsystem.
     *
     * @param id the id of the entity
     */
    @Override
    public void updateIssues(Long id) {
        log.debug("Request to update issue in Subsystem : {}", id);
        try {
            subsystemUpdateService.updateIssues(subsystemRepository.findOne(id));
        } catch (Exception e) {
            log.error("Failed to update issue in Subsystem : {}. Exception message: {}", id, e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Update information of the "id" subsystem.
     *
     * @param id the id of the entity
     */
    @Override
    public void updateInformation(Long id) {
        log.debug("Request to update information of Subsystem : {}", id);
        try {
            subsystemUpdateService.updateInformation(subsystemRepository.findOne(id));
            subsystemUpdateService.updateJobStatuses(subsystemRepository.findOne(id));
        } catch (Exception e) {
            log.error("Failed to update issue in Subsystem : {}. Exception message: {}", id, e.getMessage());
            e.printStackTrace();
        }
    }
}
