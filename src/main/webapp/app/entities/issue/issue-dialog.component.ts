import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Issue } from './issue.model';
import { IssuePopupService } from './issue-popup.service';
import { IssueService } from './issue.service';
import { ObjectStatus, ObjectStatusService } from '../object-status';
import { SubsystemObject, SubsystemObjectService } from '../subsystem-object';
import { User, UserService } from '../../shared';

@Component({
    selector: 'jhi-issue-dialog',
    templateUrl: './issue-dialog.component.html'
})
export class IssueDialogComponent implements OnInit {

    issue: Issue;
    isSaving: boolean;

    objectstatuses: ObjectStatus[];

    subsystemobjects: SubsystemObject[];

    users: User[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private issueService: IssueService,
        private objectStatusService: ObjectStatusService,
        private subsystemObjectService: SubsystemObjectService,
        private userService: UserService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.objectStatusService.query()
            .subscribe((res: HttpResponse<ObjectStatus[]>) => { this.objectstatuses = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.subsystemObjectService.query()
            .subscribe((res: HttpResponse<SubsystemObject[]>) => { this.subsystemobjects = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.userService.query()
            .subscribe((res: HttpResponse<User[]>) => { this.users = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.issue.id !== undefined) {
            this.subscribeToSaveResponse(
                this.issueService.update(this.issue));
        } else {
            this.subscribeToSaveResponse(
                this.issueService.create(this.issue));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Issue>>) {
        result.subscribe((res: HttpResponse<Issue>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Issue) {
        this.eventManager.broadcast({ name: 'issueListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackObjectStatusById(index: number, item: ObjectStatus) {
        return item.id;
    }

    trackSubsystemObjectById(index: number, item: SubsystemObject) {
        return item.id;
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-issue-popup',
    template: ''
})
export class IssuePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private issuePopupService: IssuePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.issuePopupService
                    .open(IssueDialogComponent as Component, params['id']);
            } else {
                this.issuePopupService
                    .open(IssueDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
