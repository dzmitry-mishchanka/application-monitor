import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ObjectStatus } from './object-status.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ObjectStatus>;

@Injectable()
export class ObjectStatusService {

    private resourceUrl =  SERVER_API_URL + 'api/object-statuses';

    constructor(private http: HttpClient) { }

    create(objectStatus: ObjectStatus): Observable<EntityResponseType> {
        const copy = this.convert(objectStatus);
        return this.http.post<ObjectStatus>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(objectStatus: ObjectStatus): Observable<EntityResponseType> {
        const copy = this.convert(objectStatus);
        return this.http.put<ObjectStatus>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ObjectStatus>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ObjectStatus[]>> {
        const options = createRequestOption(req);
        return this.http.get<ObjectStatus[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ObjectStatus[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ObjectStatus = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ObjectStatus[]>): HttpResponse<ObjectStatus[]> {
        const jsonResponse: ObjectStatus[] = res.body;
        const body: ObjectStatus[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ObjectStatus.
     */
    private convertItemFromServer(objectStatus: ObjectStatus): ObjectStatus {
        const copy: ObjectStatus = Object.assign({}, objectStatus);
        return copy;
    }

    /**
     * Convert a ObjectStatus to a JSON which can be sent to the server.
     */
    private convert(objectStatus: ObjectStatus): ObjectStatus {
        const copy: ObjectStatus = Object.assign({}, objectStatus);
        return copy;
    }
}
