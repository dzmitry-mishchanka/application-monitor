package com.mishchenko.appmonitor.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the SubsystemParameter entity.
 */
public class SubsystemParameterDTO implements Serializable {

    private Long id;

    private String parameterKey;

    private String parameterValue;

    private Long subsystemId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParameterKey() {
        return parameterKey;
    }

    public void setParameterKey(String parameterKey) {
        this.parameterKey = parameterKey;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    public Long getSubsystemId() {
        return subsystemId;
    }

    public void setSubsystemId(Long subsystemId) {
        this.subsystemId = subsystemId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SubsystemParameterDTO subsystemParameterDTO = (SubsystemParameterDTO) o;
        if(subsystemParameterDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), subsystemParameterDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SubsystemParameterDTO{" +
            "id=" + getId() +
            ", parameterKey='" + getParameterKey() + "'" +
            ", parameterValue='" + getParameterValue() + "'" +
            "}";
    }
}
