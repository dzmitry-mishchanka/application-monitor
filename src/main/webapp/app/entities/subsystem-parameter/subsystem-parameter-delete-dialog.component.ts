import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SubsystemParameter } from './subsystem-parameter.model';
import { SubsystemParameterPopupService } from './subsystem-parameter-popup.service';
import { SubsystemParameterService } from './subsystem-parameter.service';

@Component({
    selector: 'jhi-subsystem-parameter-delete-dialog',
    templateUrl: './subsystem-parameter-delete-dialog.component.html'
})
export class SubsystemParameterDeleteDialogComponent {

    subsystemParameter: SubsystemParameter;

    constructor(
        private subsystemParameterService: SubsystemParameterService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.subsystemParameterService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'subsystemParameterListModification',
                content: 'Deleted an subsystemParameter'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-subsystem-parameter-delete-popup',
    template: ''
})
export class SubsystemParameterDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private subsystemParameterPopupService: SubsystemParameterPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.subsystemParameterPopupService
                .open(SubsystemParameterDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
