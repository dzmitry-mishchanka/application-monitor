import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { SubsystemObjectComponent } from './subsystem-object.component';
import { SubsystemObjectDetailComponent } from './subsystem-object-detail.component';
import { SubsystemObjectPopupComponent } from './subsystem-object-dialog.component';
import { SubsystemObjectDeletePopupComponent } from './subsystem-object-delete-dialog.component';

@Injectable()
export class SubsystemObjectResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const subsystemObjectRoute: Routes = [
    {
        path: 'subsystem-object',
        component: SubsystemObjectComponent,
        resolve: {
            'pagingParams': SubsystemObjectResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.subsystemObject.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'subsystem-object/:id',
        component: SubsystemObjectDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.subsystemObject.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const subsystemObjectPopupRoute: Routes = [
    {
        path: 'subsystem-object-new',
        component: SubsystemObjectPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.subsystemObject.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'subsystem-object/:id/edit',
        component: SubsystemObjectPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.subsystemObject.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'subsystem-object/:id/delete',
        component: SubsystemObjectDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.subsystemObject.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
