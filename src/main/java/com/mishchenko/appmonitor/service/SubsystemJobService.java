package com.mishchenko.appmonitor.service;

import java.util.List;

import com.mishchenko.appmonitor.service.dto.SubsystemJobDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing SubsystemJob.
 */
public interface SubsystemJobService {

    /**
     * Save a subsystemJob.
     *
     * @param subsystemJobDTO the entity to save
     * @return the persisted entity
     */
    SubsystemJobDTO save(SubsystemJobDTO subsystemJobDTO);

    /**
     * Get all the subsystemJobs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<SubsystemJobDTO> findAll(Pageable pageable);

    /**
     * Get the "id" subsystemJob.
     *
     * @param id the id of the entity
     * @return the entity
     */
    SubsystemJobDTO findOne(Long id);

    /**
     * Get all subsystemJobs for one subsystem.
     *
     * @param id the id of the subsystem
     * @return the entity
     */
    List<SubsystemJobDTO> findAllBySubsystemId(Long id);

    /**
     * Delete the "id" subsystemJob.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
