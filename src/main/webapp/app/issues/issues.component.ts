import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { Subscription } from 'rxjs/Subscription';
import { Issue, IssueService } from '../entities/issue';
import { Subsystem, SubsystemService } from '../entities/subsystem';
import { Account, Principal } from '../shared';

@Component({
    selector: 'jhi-issues',
    templateUrl: './issues.component.html',
    styleUrls: [
        'issues.css'
    ]

})
export class IssuesComponent implements OnInit {

    account: Account;
    modalRef: NgbModalRef;
    subscription: Subscription;
    subsystemId: number;
    subsystem: Subsystem;
    issues: Issue[];
    isUpdating: boolean;

    constructor(
        private principal: Principal,
        private eventManager: JhiEventManager,
        private jhiAlertService: JhiAlertService,
        private route: ActivatedRoute,
        private issueService: IssueService,
        private subsystemService: SubsystemService
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.subsystemId = params['subsystemId'];
            this.subsystemService.find(this.subsystemId).subscribe(
                (res: HttpResponse<Subsystem>) => {
                    this.subsystem = res.body;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
            this.loadIssues();
        });
        this.principal.identity().then((account) => {
            this.account = account;
        });
    }

    private loadIssues() {
        this.issueService.findBySubsystemId(this.subsystemId).subscribe(
            (res: HttpResponse<Issue[]>) => {
                this.issues = res.body;
                console.log(this.issues);
                this.issues.sort((a, b): number => this.compare(a, b));
                console.log(this.issues);
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    private updateIssues(subsystemId: number) {
        this.isUpdating = false;
        this.subsystemService.updateIssues(subsystemId).subscribe(
            () => {
                this.loadIssues();
                this.isUpdating = true;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    private compare(a, b) {
        if (a.resolvedAt === null && b.resolvedAt !== null) {
            return -1;
        }
        if (a.resolvedAt !== null && b.resolvedAt === null) {
            return 1;
        }
        return 0;
    }
}
