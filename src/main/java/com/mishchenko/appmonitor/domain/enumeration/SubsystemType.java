package com.mishchenko.appmonitor.domain.enumeration;

/**
 * The SubsystemType enumeration.
 */
public enum SubsystemType {
    IMS, MQ, OPC, DB2
}
