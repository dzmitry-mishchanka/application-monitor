/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { AppmonitorTestModule } from '../../../test.module';
import { SubsystemObjectDetailComponent } from '../../../../../../main/webapp/app/entities/subsystem-object/subsystem-object-detail.component';
import { SubsystemObjectService } from '../../../../../../main/webapp/app/entities/subsystem-object/subsystem-object.service';
import { SubsystemObject } from '../../../../../../main/webapp/app/entities/subsystem-object/subsystem-object.model';

describe('Component Tests', () => {

    describe('SubsystemObject Management Detail Component', () => {
        let comp: SubsystemObjectDetailComponent;
        let fixture: ComponentFixture<SubsystemObjectDetailComponent>;
        let service: SubsystemObjectService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [AppmonitorTestModule],
                declarations: [SubsystemObjectDetailComponent],
                providers: [
                    SubsystemObjectService
                ]
            })
            .overrideTemplate(SubsystemObjectDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SubsystemObjectDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SubsystemObjectService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new SubsystemObject(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.subsystemObject).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
