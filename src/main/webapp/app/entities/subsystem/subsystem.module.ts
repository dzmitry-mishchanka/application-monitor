import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppmonitorSharedModule } from '../../shared';
import {
    SubsystemService,
    SubsystemPopupService,
    SubsystemComponent,
    SubsystemDetailComponent,
    SubsystemDialogComponent,
    SubsystemPopupComponent,
    SubsystemDeletePopupComponent,
    SubsystemDeleteDialogComponent,
    subsystemRoute,
    subsystemPopupRoute,
} from './';

const ENTITY_STATES = [
    ...subsystemRoute,
    ...subsystemPopupRoute,
];

@NgModule({
    imports: [
        AppmonitorSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SubsystemComponent,
        SubsystemDetailComponent,
        SubsystemDialogComponent,
        SubsystemDeleteDialogComponent,
        SubsystemPopupComponent,
        SubsystemDeletePopupComponent,
    ],
    entryComponents: [
        SubsystemComponent,
        SubsystemDialogComponent,
        SubsystemPopupComponent,
        SubsystemDeleteDialogComponent,
        SubsystemDeletePopupComponent,
    ],
    providers: [
        SubsystemService,
        SubsystemPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppmonitorSubsystemModule {}
