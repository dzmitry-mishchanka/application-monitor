/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { AppmonitorTestModule } from '../../../test.module';
import { ObjectTypeDetailComponent } from '../../../../../../main/webapp/app/entities/object-type/object-type-detail.component';
import { ObjectTypeService } from '../../../../../../main/webapp/app/entities/object-type/object-type.service';
import { ObjectType } from '../../../../../../main/webapp/app/entities/object-type/object-type.model';

describe('Component Tests', () => {

    describe('ObjectType Management Detail Component', () => {
        let comp: ObjectTypeDetailComponent;
        let fixture: ComponentFixture<ObjectTypeDetailComponent>;
        let service: ObjectTypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [AppmonitorTestModule],
                declarations: [ObjectTypeDetailComponent],
                providers: [
                    ObjectTypeService
                ]
            })
            .overrideTemplate(ObjectTypeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ObjectTypeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ObjectTypeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new ObjectType(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.objectType).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
