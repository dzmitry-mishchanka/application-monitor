package com.mishchenko.appmonitor.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mishchenko.appmonitor.service.SubsystemParameterService;
import com.mishchenko.appmonitor.web.rest.errors.BadRequestAlertException;
import com.mishchenko.appmonitor.web.rest.util.HeaderUtil;
import com.mishchenko.appmonitor.service.dto.SubsystemParameterDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SubsystemParameter.
 */
@RestController
@RequestMapping("/api")
public class SubsystemParameterResource {

    private final Logger log = LoggerFactory.getLogger(SubsystemParameterResource.class);

    private static final String ENTITY_NAME = "subsystemParameter";

    private final SubsystemParameterService subsystemParameterService;

    public SubsystemParameterResource(SubsystemParameterService subsystemParameterService) {
        this.subsystemParameterService = subsystemParameterService;
    }

    /**
     * POST  /subsystem-parameters : Create a new subsystemParameter.
     *
     * @param subsystemParameterDTO the subsystemParameterDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new subsystemParameterDTO, or with status 400 (Bad Request) if the subsystemParameter has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/subsystem-parameters")
    @Timed
    public ResponseEntity<SubsystemParameterDTO> createSubsystemParameter(@RequestBody SubsystemParameterDTO subsystemParameterDTO) throws URISyntaxException {
        log.debug("REST request to save SubsystemParameter : {}", subsystemParameterDTO);
        if (subsystemParameterDTO.getId() != null) {
            throw new BadRequestAlertException("A new subsystemParameter cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SubsystemParameterDTO result = subsystemParameterService.save(subsystemParameterDTO);
        return ResponseEntity.created(new URI("/api/subsystem-parameters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /subsystem-parameters : Updates an existing subsystemParameter.
     *
     * @param subsystemParameterDTO the subsystemParameterDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated subsystemParameterDTO,
     * or with status 400 (Bad Request) if the subsystemParameterDTO is not valid,
     * or with status 500 (Internal Server Error) if the subsystemParameterDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/subsystem-parameters")
    @Timed
    public ResponseEntity<SubsystemParameterDTO> updateSubsystemParameter(@RequestBody SubsystemParameterDTO subsystemParameterDTO) throws URISyntaxException {
        log.debug("REST request to update SubsystemParameter : {}", subsystemParameterDTO);
        if (subsystemParameterDTO.getId() == null) {
            return createSubsystemParameter(subsystemParameterDTO);
        }
        SubsystemParameterDTO result = subsystemParameterService.save(subsystemParameterDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, subsystemParameterDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /subsystem-parameters : get all the subsystemParameters.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of subsystemParameters in body
     */
    @GetMapping("/subsystem-parameters")
    @Timed
    public List<SubsystemParameterDTO> getAllSubsystemParameters() {
        log.debug("REST request to get all SubsystemParameters");
        return subsystemParameterService.findAll();
        }

    /**
     * GET  /subsystem-parameters/:id : get the "id" subsystemParameter.
     *
     * @param id the id of the subsystemParameterDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the subsystemParameterDTO, or with status 404 (Not Found)
     */
    @GetMapping("/subsystem-parameters/{id}")
    @Timed
    public ResponseEntity<SubsystemParameterDTO> getSubsystemParameter(@PathVariable Long id) {
        log.debug("REST request to get SubsystemParameter : {}", id);
        SubsystemParameterDTO subsystemParameterDTO = subsystemParameterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(subsystemParameterDTO));
    }

    /**
     * DELETE  /subsystem-parameters/:id : delete the "id" subsystemParameter.
     *
     * @param id the id of the subsystemParameterDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/subsystem-parameters/{id}")
    @Timed
    public ResponseEntity<Void> deleteSubsystemParameter(@PathVariable Long id) {
        log.debug("REST request to delete SubsystemParameter : {}", id);
        subsystemParameterService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
