package com.mishchenko.appmonitor.zos.util;

/**
 * Simple representation of z/OS object with name, type and status.
 */
public class ZObject {

    private String name;

    private String type;

    private String status;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "ZosObject{" + "name='" + getName() + "', type='" + getType() + "', status='" + getStatus() + "'}";
    }
}