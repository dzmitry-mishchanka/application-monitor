/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { AppmonitorTestModule } from '../../../test.module';
import { ObjectStatusDetailComponent } from '../../../../../../main/webapp/app/entities/object-status/object-status-detail.component';
import { ObjectStatusService } from '../../../../../../main/webapp/app/entities/object-status/object-status.service';
import { ObjectStatus } from '../../../../../../main/webapp/app/entities/object-status/object-status.model';

describe('Component Tests', () => {

    describe('ObjectStatus Management Detail Component', () => {
        let comp: ObjectStatusDetailComponent;
        let fixture: ComponentFixture<ObjectStatusDetailComponent>;
        let service: ObjectStatusService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [AppmonitorTestModule],
                declarations: [ObjectStatusDetailComponent],
                providers: [
                    ObjectStatusService
                ]
            })
            .overrideTemplate(ObjectStatusDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ObjectStatusDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ObjectStatusService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new ObjectStatus(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.objectStatus).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
