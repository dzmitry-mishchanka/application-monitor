import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppmonitorSharedModule } from '../shared';
import { SUBSYSTEM_DETAIL_ROUTE, SubsystemDetailComponent } from './';
import { SubsystemJobLogLineComponent } from './subsystem-job-log-modal/subsystem-job-log-line/subsystem-job-log-line.component';
import { SubsystemJobLogModalComponent } from './subsystem-job-log-modal/subsystem-job-log-modal.component';

@NgModule({
    imports: [
        AppmonitorSharedModule,
        RouterModule.forChild([SUBSYSTEM_DETAIL_ROUTE])
    ],
    declarations: [
        SubsystemDetailComponent,
        SubsystemJobLogModalComponent,
        SubsystemJobLogLineComponent
    ],
    entryComponents: [
        SubsystemJobLogModalComponent
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppmonitorSubsystemDetailModule { }
