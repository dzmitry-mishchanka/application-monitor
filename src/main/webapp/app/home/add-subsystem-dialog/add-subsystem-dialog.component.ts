import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { Observable } from 'rxjs/Observable';
import { Subsystem } from '../../entities/subsystem/subsystem.model';
import { SubsystemService } from '../../entities/subsystem/subsystem.service';

@Component({
    selector: 'jhi-add-subsystem-dialog',
    templateUrl: './add-subsystem-dialog.component.html',
    styleUrls: [
        '../home.css'
    ]
})
export class AddSubsystemDialogComponent implements OnInit {

    subsystem: Subsystem;
    isSaving: boolean;
    hostname: string;
    port: number;
    userId: string;
    password: string;
    imsDatastore: string;
    imsExitIdentifier: string;
    jobAccountingInfo: string;
    jobClass: string;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private subsystemService: SubsystemService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.subsystem = new Subsystem();
        this.isSaving = false;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.subsystem.id !== undefined) {
            this.subscribeToSaveResponse(
                this.subsystemService.update(this.subsystem));
        } else {
            this.subscribeToSaveResponse(
                this.subsystemService.create(this.subsystem));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Subsystem>>) {
        result.subscribe((res: HttpResponse<Subsystem>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Subsystem) {
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
