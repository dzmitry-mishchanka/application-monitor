package com.mishchenko.appmonitor.zos.scheduler;

import javax.transaction.Transactional;

import com.mishchenko.appmonitor.domain.Subsystem;
import com.mishchenko.appmonitor.repository.SubsystemRepository;
import com.mishchenko.appmonitor.zos.service.SubsystemUpdateService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class UpdateScheduler {

    @Autowired
    private SubsystemUpdateService subsystemUpdateService;

    @Autowired
    private SubsystemRepository subsystemRepository;

    // @Scheduled(fixedDelay = 600000)
    // void updateIssues() {
    // try {
    // Subsystem subsystem = subsystemRepository.findOne(1L);
    // imsSubsystemServiceImpl.updateIssues(subsystem);
    // } catch (Exception e) {
    // e.printStackTrace();
    // }
    // }

    // @Scheduled(fixedDelay = 600000)
    // void updateInformation() {
    // try {
    // Subsystem subsystem = subsystemRepository.findOne(1L);
    // imsSubsystemServiceImpl.updateInformation(subsystem);
    // } catch (Exception e) {
    // e.printStackTrace();
    // }
    // }

    // @Scheduled(fixedDelay = 240000)
    // void updateJobs() {
    //     try {
    //         Subsystem subsystem = subsystemRepository.findOne(1L);
    //         subsystemUpdateService.updateJobStatuses(subsystem);
    //     } catch (Exception e) {
    //         e.printStackTrace();
    //     }
    // }
}
